﻿USE compañias;



-- Ejercicio 1
-- Crear las claves ajenas con el nombre colocado en el diagrama de relaciones

  ALTER TABLE trabaja
    ADD CONSTRAINT FK_trabaja_compañia_nombre FOREIGN KEY (compañia)
    REFERENCES compañia(nombre) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE trabaja
 ADD CONSTRAINT FK_trabaja_persona_nombre FOREIGN KEY (persona)
    REFERENCES persona(nombre) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE compañia
 ADD CONSTRAINT FK_compañia_ciudad_nombre FOREIGN KEY (ciudad)
    REFERENCES ciudad(nombre) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE persona
 ADD CONSTRAINT FK_persona_ciudad_nombre FOREIGN KEY (ciudad)
    REFERENCES ciudad(nombre) ON DELETE NO ACTION ON UPDATE NO ACTION;


ALTER TABLE supervisa
 ADD CONSTRAINT FK_supervisa_persona_nombre FOREIGN KEY (supervisor)
    REFERENCES persona(nombre) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE supervisa
 ADD CONSTRAINT FK_supervisa_persona_nombre1 FOREIGN KEY (persona)
    REFERENCES persona(nombre) ON DELETE NO ACTION ON UPDATE NO ACTION;


  -- Ejercicio 2
-- Indicar el número de ciudades que hay en la tabla ciudades

    -- CREAR LA VISTA 

CREATE OR REPLACE VIEW ejercicio2 AS
SELECT
  COUNT(*)cuenta_ciudades
FROM ciudad c;


-- llamar la vista

  SELECT * FROM ejercicio2 e;




  -- Ejercicio 3
-- Indicar el nombre de las ciudades que tengan una población por encima de la
-- población media

-- crear la vista

    CREATE OR REPLACE VIEW ejercicio3 AS
      SELECT 
      * FROM ciudad c
      WHERE c.población > (SELECT AVG(c1.población) FROM ciudad c1);



-- Llama a la vista 
  SELECT * FROM ejercicio3 e;




  -- Ejercicio 4
-- Indicar el nombre de las ciudades que tengan una población por debajo de la
-- población media

CREATE OR REPLACE VIEW ejercicio4 AS
  SELECT 
  c.nombre FROM ciudad c
     WHERE c.población <(SELECT AVG(c1.población) FROM ciudad c1);

SELECT * FROM ejercicio4 e;


  -- Ejercicio 5
-- Indicar el nombre de la ciudad con la población máxima

    CREATE OR REPLACE VIEW ejercicio5 AS
      SELECT
      * FROM ciudad c
      WHERE c.población=(SELECT MAX(c1.población) FROM ciudad c1);


    -- LLAMA LA VISTA

SELECT * FROM ejercicio5 e;







  -- Ejercicio 6
-- Indicar el número de ciudades que tengan una población por encima de la población
-- media

   CREATE OR REPLACE VIEW ejercicio6 AS
    SELECT COUNT(*) FROM ciudad c
    WHERE c.población>(SELECT AVG(c1.población) FROM ciudad c1);

  SELECT * FROM ejercicio6 e;





  -- Ejercicio 7
-- Indicarme el número de personas que viven en cada ciudad

    CREATE OR REPLACE VIEW ejercicio7 AS
      SELECT 
      p.ciudad, COUNT(*) FROM persona p JOIN ciudad c ON p.ciudad = c.nombre
      GROUP BY p.ciudad;

    SELECT * FROM ejercicio7 e;






  -- Ejercicio 8


-- Utilizando la tabla trabaja indicarme cuantas personas trabajan en cada una de
-- las compañías 

    CREATE OR REPLACE VIEW ejercicio8 AS 
      SELECT t.compañia, COUNT(*) 
        FROM trabaja t
      GROUP BY 
      t.compañia;


    SELECT * FROM ejercicio8 e;



  -- Ejercicio 9
-- Indicarme la compañía que más trabajadores tiene

    CREATE OR REPLACE VIEW ejercicio9 AS
      SELECT t.compañia, COUNT(*)  AS  cuenta FROM trabaja t
      GROUP BY
       t.compañia;

    SELECT * FROM ejercicio9 e;


  -- Ejercicio 10
-- Listar el nombre de las personas, la calle donde vive y la población de la ciudad
-- donde vive

CREATE OR REPLACE VIEW ejercicio10 AS
  SELECT p.nombre, p.calle, c.población  
  FROM 
  ciudad c
  JOIN persona p 
  ON c.nombre = p.ciudad;

SELECT * FROM ejercicio10 e;







  -- Ejercicio 11
-- Listar el nombre de las personas, la ciudad donde vive y la ciudad donde está la
-- compañía para la que trabaja


    CREATE OR REPLACE VIEW ejercicio11 AS
 
      SELECT p.nombre,p.ciudad vive,c.ciudad trabaja  
        FROM persona p 
        JOIN trabaja t ON p.nombre=t.persona 
        JOIN compañia c ON c.nombre=t.compañia;


    SELECT * FROM ejercicio11 e;
     


